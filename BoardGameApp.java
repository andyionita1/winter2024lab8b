import java.util.Scanner;

public class BoardGameApp {
	
	public static void main(String[] args) {
		
		Board board = new Board();
		
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Welcome to the board game!");
		
		int numCastles = 7;
		int turns = 0;
		
		while (numCastles > 0 && turns < 8) {
			System.out.println(board);
			
			System.out.println("Castles: " + numCastles);
			System.out.println("Turns: " + turns + "/8");
			
			System.out.println("Enter the column you want to place the castle in");
		
			int col = reader.nextInt();
		
			System.out.println("Enter the row you want to place the castle in");
		
			int row = reader.nextInt();
			
			int tokenReturnValue = board.placeToken(row,col);
			
			while(tokenReturnValue < 0) {
				
				System.out.println("Re-enter the column");
				
				col = reader.nextInt();
				
				System.out.println("Re-enter the row");
				
				row = reader.nextInt();
				
			}
			
			if (tokenReturnValue == 1) {
					System.out.println("There is a wall there.");
					turns++;
			} else 
				if (tokenReturnValue == 0) {
					System.out.println("A castle tile was successfully placed there!");
					numCastles--;
					turns++;
				}
			}
			
			System.out.println(board);
		
			if (numCastles == 0) {
				System.out.println("You won!");
			} else {
				System.out.println("You lost!");
			}
	}
}
		