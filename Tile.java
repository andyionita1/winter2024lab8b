public enum Tile {
	BLANK("_"),
	HIDDEN_WALL("_"),
	WALL("W"),
	CASTLE("C");
	
	private final String name;
	
	private Tile(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}

}