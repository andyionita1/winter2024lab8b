import java.util.Random;

public class Board {
	
	private Tile[][] grid;
	private final int rowLength = 5;
	private final int colLength = 5;
	
	public Board() {
		
		Random randomizer = new Random();
		
		grid = new Tile[rowLength][colLength];
		
		for (int row = 0; row < rowLength; row++) {
			for (int column = 0; column < colLength; column++) {
				
				grid[row][column] = Tile.BLANK;
				
				int randomRow = randomizer.nextInt(rowLength);
				int randomCol = randomizer.nextInt(colLength);
				grid[randomRow][randomCol] = Tile.HIDDEN_WALL;
			}
		}
	}
	
	public String toString() {
		String boardString = "  ";
		
		for (int col = 0; col < colLength; col++) {
			boardString += col + " ";
		}
		
		boardString += "\n";

		for (int row = 0; row < rowLength; row++) {
			boardString += row + " ";
			for (int col = 0; col < colLength; col++) {
				boardString += grid[row][col].getName() + " ";
			}
			boardString += "\n";
		}
		
		return boardString;
	}
	
	public int placeToken(int row, int col) {
		if (row < 0 || row >= rowLength || col < 0 || col >= colLength) {
			return -2;
		}
		
		if (grid[row][col] == Tile.CASTLE || grid[row][col] == Tile.WALL) {
			return -1;
		}
		
		if (grid[row][col] == Tile.HIDDEN_WALL) {
			grid[row][col] = Tile.WALL;
			return 1;
		}
		
		if (grid[row][col] == Tile.BLANK) {
			grid[row][col] = Tile.CASTLE;
			return 0;
		}
		
		return 3;
		
	}
	
		
}
